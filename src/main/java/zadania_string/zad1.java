package zadania_string;

import java.util.Scanner;

public class zad1 {
     /*
    Napisz program, który odczytuje wyraz i sprawdza czy pierwsza litera to J

    Dane:
    Jacek
    Wynik:
    true

    Dane2:
    Tomek
    Wynik:
    false */

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String name = sc.next();
        String name2 = sc.next();

        System.out.println(name);

        if (name.startsWith("J")) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

        System.out.println(name2);


        if (name2.startsWith("J")) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
