package zadania_string;

import java.util.Scanner;

public class zad2 {
    /* Odczytaj wyraz i wypisz na ekran wartość true lub false w
       zależności od tego czy wyraz zawiera w sobie napis pies.*/

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String word = sc.next();

        System.out.println(word);

        if(word.contains("pies")){
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }
    }
}
