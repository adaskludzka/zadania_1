package zadania_petle;

import java.util.Scanner;

public class zad5 {

    /*Napisz program, który prosi o podanie poprawnego hasła( hasło to Polska), tak
      długo jak użytkownik nie odgadnie hasła wyświetlany jest komunikat podaj
      poprawne hasło. */

    public static void main(String[] args) {

        String hasło = "Polska";
        String a;

        Scanner sc = new Scanner(System.in);
        boolean poprawne_haslo = false;

        while (poprawne_haslo == false) {
            System.out.println("Podaj poprawne hasło:");
            a = sc.next();
            if (a.equals(hasło)) {
                System.out.println("Hasło jest poprawne.");
                poprawne_haslo = true;
            } else {
                System.out.println("Nieprawidłowe hasło");
            }


        }
    }
}
