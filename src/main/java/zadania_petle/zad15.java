package zadania_petle;

import java.util.Scanner;

public class zad15 {
    public static void main(String[] args) {

        //Napisz program, który oblicza największy wspólny dzielnik dwóch liczb.

        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();

        while (a != 0 && b != 0) {
            if (a > b) {
                a = a % b;
            } else {
                b = b % a;
            }
        }
        if (b == 0) {
            System.out.println(a);
        } else {
            System.out.println(b);
        }

    }

}
