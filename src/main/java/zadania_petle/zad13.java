package zadania_petle;

import java.util.Scanner;

public class zad13 {
     /*Stwórz program, który odczytuje dany napis i wypisuje ile razy w danym napisie
      występują małe litery. Przykładowo dla napisu: aAaaBssk wynikiem powinno być
      6 (małe a występuje 3 razy, s występuje 2 razy, k występuje 1 raz).  */

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String a = sc.next();
        char[] b = a.toCharArray();

        int male_litery = 0;
        for (int i = 0; i < b.length; i++) {
            boolean c = Character.isLowerCase(b[i]);
            if (c) {
                male_litery++;
            }
        }
        System.out.println(male_litery);
    }
}
