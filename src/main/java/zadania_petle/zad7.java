package zadania_petle;

import java.util.Scanner;

public class zad7 {

    /* Napisz program który wygeneruje za pomocą (wielkość wieżyczki podaje
       użytkownik)
       a )wieżyczkę
       *
       **
       ***
       ****
       b) choinkę
        *
       ***
      *****
     *******
     */

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj wysokość wieżyczki:");
        int a = sc.nextInt();


        for(int i = 1; i<=a; i++){
            for(int j = 0; j<(i-1); j++)
                System.out.print("*");
            System.out.println(" ");
        }


        Scanner s = new Scanner(System.in);
        System.out.println("Podaj wysokość choinki:");
        int b = s.nextInt();


        for(int i=1; i<=b; i++){
            for(int j=0;j<b-i;j++)
                System.out.print(" ");
            for(int j=0; j<(i*2)-1; j++)
                System.out.print("*");
            System.out.println(" ");
        }
    }
}
