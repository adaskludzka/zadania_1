package zadania_petle;

import java.util.Scanner;

public class zad3 {

    /*Napisz prostą grę - zadaniem użytkownika będzie zgadnięcie liczby, którą
      zainicjujemy w programie (przykładowa liczba 600). W przypadku, gdy liczba
      będzie za duża lub za mała, użytkownik otrzyma odpowiednią podpowiedź.
      Gramy tak długo dopóki użytkownik zgadnie liczbę.*/

    public static void main(String[] args) {


        int a = 600;

        Scanner sc = new Scanner(System.in);

        int b;

        do {
            System.out.println("Wpisz liczbę:");
            b = sc.nextInt();
            if (b > a) {
                System.out.println("Ta liczba jest za duża.");
            } else if (b < a) {
                System.out.println("Ta liczba jest za mała.");
            } else if (b == a) {
                System.out.println("Zgadłeś. To jest ta liczba.");
            }


        }while(b != a);
    }
}
